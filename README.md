# Varimar


# Para instalar el Proyecto

git clone https://git@gitlab.com/pmtk/varimar.git


# Instalar complementos

npm install --global gulp dotenv browser-sync node-sass gulp-sass gulp-cssmin gulp-concat gulp-uglify mysqldump gulp-replace gulp-rename prompts del gulp-sourcemaps


# Enlace de modulos de node

npm link gulp dotenv browser-sync node-sass gulp-sass gulp-cssmin gulp-concat gulp-uglify mysqldump gulp-replace gulp-rename prompts del gulp-sourcemaps


# Comandos para modificar

gulp iniciodev


gulp continuadev


# Comando para subir cambios con gulpfile

gulp guarda

gulp despliegue


# Subir cambios manualmente

git add -A

git commit -m "texto commit descriptivo"

git push https://git@gitlab.com/pmtk/varimar.git


# ERROR SI DA:

git pull --rebase origin master master

git push origin master
