<?php
/*
Template Name: Inicio
*/
get_header();
//BODY ////////////////////////////////////////////
?>
<main class="main">
  <div class="contenedor">
  <?php
    if(have_posts())
    {
      while( have_posts() )
      {
        the_post();
        the_content();
//bloques secciones contenido flexible

       
      ?>
      <div class="bloques">
      <div class="fondo">
        <!-- <img src="/wp-content/uploads/2020/09/fondo.png"> -->
      </div>
       <?php 
//bloque productos nuevos
      $bloq_new = carbon_get_the_post_meta('crb_new');
      if($bloq_new == 'yes'){?>
        <section class="nuevos">
          <div class="container">
            <div class="titulo">¡NOVEDADES!</div>
            <div class="pro-nuevos taphover">
              <?php echo do_shortcode('[products limit="3" columns="3" orderby="id" order="DESC" visibility="visible"]'); ?>
            </div>
          </div>
        </section>
     <?php } //cierra bloque productos nuevos
//bloque imagen o video + texto
      $bloq_text_img = carbon_get_the_post_meta('crb_text');
      if($bloq_text_img){?>
        <section class="video-text <?php echo carbon_get_the_post_meta('crb_class_text'); ?>">
          <div class="container">
            <div class="col-img">
              <img src="<?php echo carbon_get_the_post_meta('image'); ?>">
            </div>
            <div class="col-text">
              <span class="tit"><?php echo carbon_get_the_post_meta('crb_tit_text'); ?></span>
              <?php echo carbon_get_the_post_meta('crb_text'); ?>
            </div>
          </div>
        </section>
      <?php }//cierra bloque imagen o video + texto
//bloque titulo + shortcode
      $bloq_shortcode = carbon_get_the_post_meta('crb_shortcode');
      if($bloq_shortcode){?>
        <section class="<?php echo carbon_get_the_post_meta('crb_class_shortcode'); ?>">
          <div class="container">
            <h3 class="titulo"><?php echo carbon_get_the_post_meta('crb_tit_shortcode'); ?></h3>
            <div class="content-shortcode taphover">
              <?php echo do_shortcode($bloq_shortcode); ?>
            </div>
          </div>
        </section>
     <?php } //cierra bloque titulo + shortcode  
//bloque iconos       
       $bloq_iconos = carbon_get_the_post_meta('crb_iconos');
       if($bloq_iconos){?>
        <section class="bloque-iconos">
        <div class="container">
          <?php foreach($bloq_iconos as $bloque_icono){?>
            <div class="icono taphover">
              <a href="<?php echo esc_url($bloque_icono['crb_icono_link']); ?>" alt="<?php echo $bloque_icono['title']; ?>" title="<?php echo $bloque_icono['title']; ?>">
                <div class="ico-img">
                  <img src="<?php echo $bloque_icono['image'];  ?>">
                </div>
                <div class="ico-tit">
                  <?php echo $bloque_icono['title']; ?>
                </div>
                <div class="ico-text">
                  <p><?php echo $bloque_icono['descripcion']; ?></p>
                </div>
              </a>
            </div>
          <?php }
        ?>
        </div>
        </section>
      <?php } //cierra bloque iconos
//bloque carrusel destacados
    $bloq_desta =  carbon_get_the_post_meta('crb_dest'); 
    if($bloq_desta == 'yes'){ ?>
      <section class="destacados">
        <div class="container">
          <div class="titulo"><?php echo carbon_get_the_post_meta('tit_carrusel'); ?></div>
            <div class="pro-destacados taphover">
              <?php echo do_shortcode('[products limit="0" columns="1" visibility="featured" ]'); ?>
            </div>
        </div>
      </section>
    <?php }//fin bloque carrusel destacados
     ?>
        
      </div>
    <?php }//cierra while principal
  }else{
    echo 'Lo siento, la página no ha sido encontrada';
  }//cierra if principal
   ?>
   </div>
 </main>
<?php
get_footer();
?>
<script>
$('div.taphover').on('touchstart', function (e) {
    'use strict'; //satisfy code inspectors
    var link = $(this); //preselect the link
    if (link.hasClass('hover')) {
        return true;
    } else {
        link.addClass('hover');
        $('div.taphover').not(this).removeClass('hover');
        e.preventDefault();
        return false; //extra, and to make sure the function has consistent return points
    }
});
</script>
<script>
jQuery(document).ready(function(){
  jQuery('.destacados>.container>.pro-destacados>.woocommerce>.products').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      centerMode: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 770,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
  });

});
</script>