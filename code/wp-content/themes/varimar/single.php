<?php
get_header();
?>
<main class="pag-interna producto" >
  <section class="banner-interna">
  <img src="/wp-content/uploads/2020/07/fondo-marisco.jpg">
    <div class="inner"></div>
     <div class="titulo">
        <span class="title"><?php the_title(); ?></span>
      </div>
  </section> 
  
  <div class="breadcrumb"><?php //if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?></div>
<?php if(have_posts()){
  while( have_posts() ){
    the_post();
    //ABRE CONTENEDOR PRINCIPAL-----------------------------------------------------------------------------------   
     ?>
      <div class='contenedor'>
       <?php the_content(); ?>
       <div class="bloque widget-tienda">
          <?php  if(is_active_sidebar('widget-shop')){
            dynamic_sidebar('widget-shop');
          }
          ?>
        </div>
      </div>
    <?php }//cierra while principal   
   }else{
      echo 'Lo siento, la página no ha sido encontrada';
    } ?>
    
  
</section>
</main>
<?php
get_footer();
?>
