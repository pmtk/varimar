<?php
/*
Template Name: Contacto
*/
get_header();
?>
<main id="pag-contact" class="pag-interna">
 <section class="banner-interna">
  <img src="/wp-content/uploads/2020/07/fondo-marisco.jpg">
    <div class="inner"></div>
     <div class="titulo">
        <span class="title"><?php the_title(); ?></span>
      </div>
  </section> 
  <section class="contenedor">
    <?php if(have_posts()){
      while(have_posts()){
        the_post();
        the_content(); ?>
        
       
    
<?php
    } //cierra while ?>

<?php
  }else{
    echo 'Lo siento, la página no ha sido encontrada';
  }
?>
</section>
</main>
<script>
$('div.taphover').on('touchstart', function (e) {
    'use strict'; //satisfy code inspectors
    var link = $(this); //preselect the link
    if (link.hasClass('hover')) {
        return true;
    } else {
        link.addClass('hover');
        $('div.taphover').not(this).removeClass('hover');
        e.preventDefault();
        return false; //extra, and to make sure the function has consistent return points
    }
});
</script>
<?php
get_footer();
?>
