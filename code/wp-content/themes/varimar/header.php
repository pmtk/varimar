<?php echo '<?xml version="1.0" encoding="utf8"?>'; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2" />
    <?php wp_head(); //FUNCION PROPIA DE WP QUE INCLUYE EN EL HEAD TODOS LOS LINKS QUE UTILIZA EN SU MOTOR ?>
  </head>
  <body <?php body_class(); ?> id="<?php $post_slug; ?>">
  <header>
    <section class="top-header">
      <div class="content">
        <div class="cont-top">
          <?php 
            if(carbon_get_theme_option( 'crb_text_topheader' )){
              echo "<div class='text'>";
              echo carbon_get_theme_option( 'crb_text_topheader' );
              echo "</div>";
            } //texto del cuadro editor 
            if(carbon_get_theme_option( 'crb_show_whatsapp' )){?>
              <div class="whatsapp">
                <?php 
                  $tel_whatsapp = "";
                  if(carbon_get_theme_option('crb_whatsapp')){
                    $tel_whatsapp = carbon_get_theme_option('crb_whatsapp');
                  }else{
                    $tel_whatsapp = carbon_get_theme_option('crb_telefono_movil');
                  }
                ?>
                <a href="https://api.whatsapp.com/send?phone=+34<?php echo $tel_whatsapp;?>&amp;text=Hola%20,%20quiero%20hacer%20un%20pedido%20(^_^)%20" target="_blank" rel="nofollow noopener">Pide por Whatsapp!
                  <img src="/wp-content/uploads/2020/07/whatsapp.png" />
                </a>
              </div>
            <?php } ?>
            <div class="tel-header">
              <?php 
                $tel_header = carbon_get_theme_option('crb_telefono_head');
                $tel_movil = carbon_get_theme_option('crb_telefono_movil');
                $tel_fijo = carbon_get_theme_option('crb_telefono');
                if($tel_header == 'movil'){?>
                  <a href="tel:<?php echo $tel_movil; ?>">Pide por Teléfono<i class="fas fa-phone-volume"></i>
                  </a>
                <?php }else{?>
                  <a href="tel:<?php echo $tel_fijo; ?>">Pide por Teléfono<i class="fas fa-phone-volume"></i>
                  </a>
                <?php }
              ?>
            </div>

        </div>
      </div>
    </section>
    <section id="main-head" class="main-header">
      <div class="content  <?php echo carbon_get_theme_option('crb_logo_align'); ?>">
        <div class="logo-menu">
          <div id="logo" class="logo">
            <a href="<?php echo home_url(); ?>">
              <img class="lazyload" data-src="<?php echo carbon_get_theme_option('crb_logo'); ?>" alt="Komida para llevar" title="Komida para llevar" >
            </a>
          </div>
          <div class="menu">
            <nav id="navbar" class="menu-principal">
              <div class="contenedor">
                <?php  // FUNCION DE WP PARA IMPRIMIR EL MENU PRINCIPAL
                  wp_nav_menu(array ('theme_location' => 'menu_principal'));
                ?>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </section>
</header>
