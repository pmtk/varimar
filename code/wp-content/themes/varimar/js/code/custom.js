jQuery(document).ready(function() {
  // SCRIPT PARA TEXTO SEO TIRA DE NIÑOS  ----------------------------------

  jQuery(window).scroll(function(){
    if( jQuery(this).scrollTop() > 100 ){
      jQuery('.parallax-content:not(.large)').addClass('short');
    }
  });
  // SEO Box
  if(jQuery('#llamar-seo').length > 0){
    jQuery('#llamar-seo').on('click',function(e){
      e.preventDefault();

    if(jQuery('.parallax-content').hasClass('short')){
      jQuery('.parallax-content').removeClass('short');
      jQuery('.parallax-content').addClass('large');
      jQuery('#flecha').addClass('fa-chevron-up');
      jQuery('#flecha').removeClass('fa-chevron-down');
    } else{
      jQuery('.parallax-content').removeClass('large');
      jQuery('.parallax-content').addClass('short');
      jQuery('#flecha').addClass('fa-chevron-down');
      jQuery('#flecha').removeClass('fa-chevron-up');
    }
  });
  }

  jQuery('div.taphover').on('touchstart', function (e) {
      'use strict'; //satisfy code inspectors
      var link = jQuery(this); //preselect the link
      if (link.hasClass('hover')) {
          return true;
      } else {
          link.addClass('hover');
          jQuery('div.taphover').not(this).removeClass('hover');
          e.preventDefault();
          return false; //extra, and to make sure the function has consistent return points
      }
  });
  if(jQuery('.bloque-opiniones').length > 0){
    jQuery('.bloque-opiniones').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 15000,
      dots: false,
      arrows: true,
      adaptiveHeight: true,
      prevArrow: "<button class='slick-prev slick-arrow' aria-label='Previous' type='button' style='display: inline-block;'>Anterior</button>",
      nextArrow:"<button class='slick-next slick-arrow' aria-label='Next' type='button' style='display: inline-block;'>Siguiente</button>",
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 950,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 770,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 400,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });
  }
  if(jQuery('#menu-tratamientos').length > 0){
    jQuery('#menu-tratamientos .menu-item-has-children').unbind();
      jQuery('#menu-tratamientos .menu-item-has-children').on('click',function(){
        jQuery(this).children(".sub-menu").slideToggle();
        // e.preventDefault();
      });
  }
});
