<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;
//REGISTRO OPCIONES DEL TEMA CON CARBONFIELDS
add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
    Container::make( 'theme_options', __( 'Datos Empresa' ) )
        ->where('current_user_role', 'IN', array('administrator'))
        ->or_where('current_user_role', 'IN', array('shop_manager', 'editor'))
        ->set_icon('dashicons-edit-large')
        ->add_fields( array(
            Field::make( 'text', 'crb_direccion', 'Dirección' ),
            Field::make( 'text', 'crb_enlace_direccion', 'Enlace Google Map' ),
            Field::make( 'text', 'crb_email', __( 'Email' ) ),
            Field::make( 'text', 'crb_telefono', __( 'Teléfono Fijo' ) ),
            Field::make( 'text', 'crb_telefono_movil', __( 'Teléfono Móvil' ) ),
            Field::make( 'text', 'crb_whatsapp', __( 'Whatsapp (si es diferente al teléfono móvil)' ) ),
            Field::make( 'textarea', 'crb_horario', __( 'Horaio' ) ),
            // Field::make( 'header_scripts', 'crb_header_script', __( 'Header Script' ) ),
            // Field::make( 'footer_scripts', 'crb_footer_script', __( 'Footer Script' ) )
        ) );
       
    // Redes sociales de la empresa ---------------------------------------------------------------------------------------------------------
    Container::make( 'theme_options', __( 'Redes Sociales' ) )
        ->where('current_user_role', 'IN', array('administrator'))
        ->or_where('current_user_role', 'IN', array('shop_manager', 'editor'))
        ->set_icon('dashicons-share')
        ->set_page_file( 'theme-options' )
        ->add_fields( array(
          Field::make( 'text', 'crb_facebook_link', __( 'Facebook Link' ) ),
          Field::make( 'text', 'crb_twitter_link', __( 'Twitter Link' ) ),
          Field::make( 'text', 'crb_linkedin_link', __( 'Linkedin Link' ) ),
          Field::make( 'text', 'crb_instagram_link', __( 'Instagram Link' ) ),
          Field::make( 'text', 'crb_youtube_link', __( 'Youtube Link' ) ),
          ) );
    //cabecera ----------------------------------------------------------------------------------------------------------------------------------------
     Container::make( 'theme_options', __( 'Cabecera página' ) ) 
     ->where('current_user_role', 'IN', array('administrator'))
     ->or_where('current_user_role', 'IN', array('shop_manager', 'editor'))
     ->set_icon( 'dashicons-admin-appearance' )
     ->set_page_parent( $basic_options_container ) // reference to a top level container
     ->add_fields( array(
            Field::make( 'image', 'crb_logo', __( 'Logo' ) )
            ->set_value_type( 'url' ),
            Field::make( 'select', 'crb_logo_align', __( 'alineación logo' ) )
            ->set_options( array(
              'izquierda' => Izquierda,
              'center' => Centro,
              'derecha' => Derecha,
              ) ),
              Field::make( 'select', 'crb_telefono_head', __( 'Seleccionar teléfono a enlazar' ) )
            ->set_options( array(
              'movil' => Movil,
              'fijo' => Fijo,
              ) ),
              Field::make( 'checkbox', 'crb_show_whatsapp', __( 'Mostrar Enlace Whatsapp' ) )
                ->set_option_value( 'yes' ),
              Field::make( 'rich_text', 'crb_text_topheader', __( 'Texto top-header' ) ),
        ) ); 
}


// ============================================================================
// Shortodes Datos Generales
// ============================================================================
function shortcode_dg_direccion()
{
  //obtenermos el campo personalizado
  
  $direccion = carbon_get_theme_option('crb_direccion');
  if( $direccion )
  {
    return $direccion;
  }
}
add_shortcode( 'dg-direccion', 'shortcode_dg_direccion' );



function shortcode_dg_enlace_direccion(){
  $enlace = carbon_get_theme_option('crb_enlace_direccion');
  if($enlace){
    return $enlace;
  }
}
add_shortcode('dg-enlace_direccion', 'shortcode_dg_enlace_direccion');




/* Email */
function shortcode_dg_email()
{
  //obtenermos el campo personalizado
  $email = carbon_get_theme_option('crb_email');
  if( $email )
  {
    return $email;
  }
}
add_shortcode( 'dg-email', 'shortcode_dg_email' );



/* Teléfono Fijo */
function shortcode_dg_telefono()
{
  //obtenermos el campo personalizado
  $telefono = carbon_get_theme_option('crb_telefono');
  if( $telefono )
  {
    return $telefono;
  }
}
add_shortcode( 'dg-telefono_fijo', 'shortcode_dg_telefono' );

function shortcode_dg_telefono_movil()
{
  //obtenermos el campo personalizado
  $telefono2 =carbon_get_theme_option('crb_telefono_movil');
  if( $telefono2 )
  {
    return $telefono2;
  }
}
add_shortcode( 'dg-telefono_movil', 'shortcode_dg_telefono_movil' );

function shortcode_dg_whatsapp()
{
  //obtenermos el campo personalizado
  $telefono3 = carbon_get_theme_option('crb_whatsapp');
  if( $telefono3 )
  {
    return $telefono3;
  }else{
    $tel_movil = carbon_get_theme_option('crb_telefono_movil');
    return $tel_movil;
  }
}
add_shortcode( 'dg-whatsapp', 'shortcode_dg_whatsapp' );



function shortcode_dg_telefono_movil_2()
{
  //obtenermos el campo personalizado
  $telefono_movil2 = carbon_get_theme_option('crb_telefono_movil_2');
  if( $telefono_movil2 )
  {
    return $telefono_movil2;
  }
}
add_shortcode( 'dg-telefono_movil_2', 'shortcode_dg_telefono_movil_2' );

 /* horario */
function shortcode_dg_dias_horario()
{
  //obtenermos el campo personalizado
  $d_horario = carbon_get_theme_option('crb_horario');
  if( $d_horario )
  {
    return $d_horario;
  }
}
add_shortcode( 'dg-horario', 'shortcode_dg_dias_horario' ); 



/* Redes Sociales */
/* Facebook */
function shortcode_dg_facebook()
{
  //obtenermos el campo personalizado
  $facebook = get_field("facebook","option");
  if( $facebook )
  {
    return $facebook;
  }
}
add_shortcode( 'dg-facebook', 'shortcode_dg_facebook' );

/* Instagram */
function shortcode_dg_instagram()
{
  //obtenermos el campo personalizado
  $instagram = get_field("instagram","option");
  if( $instagram )
  {
    return $instagram;
  }
}
add_shortcode( 'dg-instagram', 'shortcode_dg_instagram' );

/* Google + */
function shortcode_dg_linkedin()
{
  //obtenermos el campo personalizado
  $linkedin = get_field("linkedin","option");
  if( $linkedin )
  {
    return $linkedin;
  }
}
add_shortcode( 'dg-linkedin', 'shortcode_dg_linkedin' );

/* Youtube */
function shortcode_dg_youtube()
{
  //obtenermos el campo personalizado
  $youtube = get_field("youtube","option");
  if( $youtube )
  {
    return $youtube;
  }
}
add_shortcode( 'dg-youtube', 'shortcode_dg_youtube' );

/* Twitter */
function shortcode_dg_twitter()
{
  //obtenermos el campo personalizado
  $twitter = get_field("twitter","option");
  if( $twitter )
  {
    return $twitter;
  }
}
add_shortcode( 'dg-twitter', 'shortcode_dg_twitter' );
/* Recoger todas las redes */
function shortcode_dg_rrss($atts)
{

  extract(shortcode_atts(array('modo' => 'normal'), $atts));
  if ($modo=='cuadrado'){
    $claseFacebook = "fab fa-facebook-square" ;
    $claseTwitter ="fab fa-twitter-square";
    $claseLinkedin ="fab fa-linkedin";
    $claseInstagram = "fab fa-instagram";
    $claseYoutube="fab fa-youtube-square";
  }
  else{
    $claseFacebook = "fab fa-facebook-f" ;
    $claseTwitter ="fab fa-twitter";
    $claseLinkedin ="fab fa-linkedin-in";
    $claseInstagram = "fab fa-instagram";
    $claseYoutube="fab fa-youtube";
  }
  $facebook = carbon_get_theme_option('crb_facebook');
  $twitter= carbon_get_theme_option('crb_twitter');
  $linkedin = carbon_get_theme_option('crb_linkedin');
  $instagram = carbon_get_theme_option('crb_instagram');
  $youtube = carbon_get_theme_option('crb_youtube');
  ob_start();
  if($facebook || $twitter || $linkedin || $instagram || $youtube){
  ?>
  <div class="redes">
  <?php
  if( $facebook ){?>
    <a href="<?php echo $facebook;?>" target="_blank" rel="nofollow noopener" title="facebook"><i class="<?php echo $claseFacebook;?>"></i></a>
    <?php
  }
  if( $twitter ){?>
    <a href="<?php echo $twitter;?>" target="_blank" rel="nofollow noopener" title="twitter"><i class="<?php echo $claseTwitter;?>"></i></a>
    <?php
  }
  if( $linkedin ){?>
    <a href="<?php echo $linkedin;?>" target="_blank" rel="nofollow noopener" title="linkedin"><i class="<?php echo $claseLinkedin;?>"></i></a>
    <?php
  }
  if( $instagram ){?>
    <a href="<?php echo $instagram;?>" target="_blank" rel="nofollow noopener" title="instagram"><i class="<?php echo $claseInstagram;?>"></i></a>
    <?php
  }
  if( $youtube ){?>
    <a href="<?php echo $youtube;?>" target="_blank" rel="nofollow noopener" title="youtube"><i class="<?php echo $claseYoutube;?>"></i></a>
    <?php
  }
  ?>
</div>
  <?php
}
  $html = ob_get_contents();
  ob_end_clean();
  return $html;
}
add_shortcode( 'dg-rrss', 'shortcode_dg_rrss' );

function recogerLogos(){
  ob_start();?>
  <section class='logos'>
   <div class='contenedor'>
   <?php
      if(have_rows('logos', 'option')){
        while(have_rows('logos', 'option')) : the_row();
        ?>
    <div class='caja-logo'>
         <img src="<?php the_sub_field('logo_marca', 'option')?>" alt="logo" title="logo" >
    </div>
        <?php
        endwhile;
      }
  ?>
  </div>
  </section>
  <?php
    $html = ob_get_contents();
    ob_end_clean();
   
    return $html;
}
add_shortcode( 'bloque_logos', 'recogerLogos' );

//añadir a categorias un banner
add_action('carbon_fields_register_fields', 'crb_attach_banner');
function crb_attach_banner(){
  Container::make('post_meta', __('Banner', 'crb'))
  ->where('post_type', '=', 'page')
  ->add_fields(array(
    Field::make('image', 'crb_banner', 'Banner')
      ->set_value_type('url'),
    Field::make('text', 'texto-banner', 'Texto banner')
      ->set_required(false)
  ));
}
//añadir nuevos productos
add_action('carbon_fields_register_fields', 'crb_attach_nuevos');
function crb_attach_nuevos(){
  Container::make('post_meta', __('Bloque últimos productos', 'crb'))
  // ->where('post_type', '=', 'page')
  ->where('post_id', '=', get_option('page_on_front'))
  ->add_fields(array(
    Field::make('checkbox', 'crb_new', 'Insertar bloque últimos productos')
      ->set_option_value('yes')
  ));
}
//añadir bloque imagen o video + texto
add_action('carbon_fields_register_fields', 'crb_attach_img_text');
function crb_attach_img_text(){
  Container::make('post_meta', __('Bloque Imagen o Video y texto', 'crb'))
  ->where('post_id', '=', get_option('page_on_front'))
  ->add_fields(array(
    Field::make('text', 'crb_class_text', 'Class bloque'),
    Field::make('text', 'crb_tit_text', 'Titulo bloque'),
    Field::make('image', 'image', 'Imagen')
      ->set_value_type('url'),
    Field::make('rich_text', 'crb_text', 'Texto bloque'),
  ));
}
//añadir bloque titulo + shortcode
add_action('carbon_fields_register_fields', 'crb_attach_shortcode');
function crb_attach_shortcode(){
  Container::make('post_meta', __('Bloque Titulo y Shortcode', 'crb'))
  ->where('post_type', '=', 'page')
  ->add_fields(array(
    Field::make('text', 'crb_class_shortcode', 'Class bloque'),
    Field::make('text', 'crb_tit_shortcode', 'Titulo bloque'),
    Field::make('text', 'crb_shortcode', 'Shortcode')
  ));
}
//añadir bloque iconos
add_action('carbon_fields_register_fields', 'crb_attach_iconos');
function crb_attach_iconos(){
  Container::make('post_meta', __('Bloque Iconos', 'crb'))
  // ->where('post_type', '=', 'page')
  ->where('post_id', '=', get_option('page_on_front'))
  ->add_fields(array(
    Field::make('complex', 'crb_iconos', 'Bloque Iconos')
      ->set_layout('tabbed-horizontal')
      ->add_fields(array(
        Field::make('image', 'image', 'Icono')
          ->set_value_type('url'),
        Field::make('text', 'title', 'Titulo'),
        Field::make('text', 'descripcion', 'Descripción'),
        Field::make('text', 'crb_icono_link', ( 'Enlace icono' ) )
        
      )),
  ));
}
//añadir carrusel productos destacados
add_action('carbon_fields_register_fields', 'crb_attach_carrusel');
function crb_attach_carrusel(){
  Container::make('post_meta', __('Carrusel Productos', 'crb'))
  // ->where('post_type', '=', 'page')
  ->where('post_id', '=', get_option('page_on_front'))
  ->add_fields(array(
    Field::make('text', 'tit_carrusel', 'Titulo'),
    Field::make('checkbox', 'crb_dest', 'Insertar carrusel productos destacados')
      ->set_option_value('yes')
  ));
}
//añadir bloques texto, texto-imagen
add_action('carbon_fields_register_fields', 'crb_attach_bloq_text');
function crb_attach_bloq_text(){
  Container::make('post_meta', __('Bloques texto o texto + imagen', 'crb'))
  ->where('post_type', '=', 'page')
  ->add_fields(array(
    Field::make('complex', 'crb_bloq_text', 'Bloque texto o texto+imagen')
      ->set_layout('tabbed-horizontal')
      ->add_fields(array(
        Field::make('radio', 'crb_class_bloq', 'Distribución bloque')
          ->add_options( array(
              'text-img' => 'Texto primero',
              'img-text' => 'Imagen primero',
              'text' => 'Solo texto',
              'img' => 'Solo imagen'
          )),
        Field::make('image', 'image_bloque', 'Imagen')
          ->set_value_type('url'),
        Field::make('text', 'title', 'Titulo'),
        Field::make('rich_text', 'texto', 'Texto')
      )),
  ));
}
?>
