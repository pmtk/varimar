<?php
/*
Template Name: Página Interna
*/
get_header();
?>
<main class="pag-interna varimar">
  <?php if(have_posts()){
    while(have_posts()){
      the_post();
//secciones internas
//sección banner principal
      $banner_int = carbon_get_the_post_meta('crb_banner');
      $text_banner = carbon_get_the_post_meta('texto-banner');
    if($banner_int){?>
      <section class="banner-interna">
        <img class="lazyload" data-src="<?php echo $banner_int; ?>" alt="<?php echo $banner_int['alt']; ?>" title="<?php echo $banner_int['title']; ?>">
        <div class="inner"></div>
        <div class="titulo">
          <span class="title">
          <?php if($text_banner){
            echo $text_banner;
          }else{
            the_title();
          }
           ?>
          </span>
        </div>
   <?php }else{?>
      <section class="banner-interna">
        <img src="/wp-content/uploads/2020/07/fondo-marisco.jpg">
        <div class="inner"></div>  
        <div class="titulo">
          <span class="title">
          <?php if($text_banner){
            echo $text_banner;
          }else{
            the_title();
          }
           ?>
          </span>
        </div>
    <?php }//fin seccion banner principal
  ?>
    </section> 
    <div class="breadcrumb"><?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?></div>

<div class="contenedor">
<?php 
  the_content(); 
/* ----------------------------------------------------------------------------------------------------------------------------- */
  //bloque texto o texto+imagen 
      $bloq_textImgs = carbon_get_the_post_meta('crb_bloq_text');
      $cont = 1;
      if($bloq_textImgs){?>
        <section class="bloques-text <?php echo carbon_get_the_post_meta('crb_class_bloq'); ?>">
          <div class="container">
          <?php 
            foreach($bloq_textImgs as $bloq_textImg){
              $img =$bloq_textImg['image_bloque'];
              $titu = $bloq_textImg['title'];
              $text = $bloq_textImg['texto'];
              $class = $bloq_textImg['crb_class_bloq'];
            ?>
              <div class="bloque bloque-<?php echo $cont; ?> <?php echo $class;?>">
                <?php if($img){?>
                  <div class="col-img">
                    <img src="<?php echo $img; ?>">
                  </div>
                <?php } 
                if($text){?>
                  <div class="col-text">
                    <?php if($titu){?>
                      <span class="tit"><?php echo $titu; ?></span>
                    <?php } 
                    echo $text; ?>
                  </div>
                <?php }?>
              </div>
              <?php $cont++;
            } ?>
          </div>
        </section>
      <?php }//cierra bloque texto o texto+imagen
//bloque titulo + shortcode
      $bloq_shortcode = carbon_get_the_post_meta('crb_shortcode');
      if($bloq_shortcode){?>
        <section class="<?php echo carbon_get_the_post_meta('crb_class_shortcode'); ?>">
          <div class="container">
            <h3 class="titulo"><?php echo carbon_get_the_post_meta('crb_tit_shortcode'); ?></h3>
            <div class="content-shortcode taphover">
              <?php echo do_shortcode($bloq_shortcode); ?>
            </div>
          </div>
        </section>
     <?php } //cierra bloque titulo + shortcode 
          
  ?>      
  </div>
  <?php  } //cierra while principal
  }else{
    echo 'Lo siento, la página no ha sido encontrada';
  } //cierra if principal
?>
</main>
<?php
get_footer();
?>
