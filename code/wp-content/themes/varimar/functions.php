<?php
//$start_wp_theme_tmp
//wp_tmp
//$end_wp_theme_tmp
?><?php



// ------------------------------------------- optimizacion


//REGISTRAR CSS
wp_register_style( 'style', get_template_directory_uri() . '/style.css',array(),'1.0');
wp_register_style( 'min', get_template_directory_uri() . '/css/min.css',array(),'1.0');
wp_register_style( 'fontawesome', get_template_directory_uri() . '/inc/fontawesome/css/all.css',array(),'1.0');
wp_register_style( 'slick', get_template_directory_uri() . '/inc/slick/slick.css',array(),'1.0');
wp_register_style( 'slick-theme', get_template_directory_uri() . '/inc/slick/slick-theme.css',array(),'1.0');
wp_register_style( 'fancybox', get_template_directory_uri() . '/inc/fancybox/jquery.fancybox.min.css',array(),'1.0');

//LLAMAR A LOS Estilos
wp_enqueue_style('style');
wp_enqueue_style('min');
wp_enqueue_style('fontawesome');
wp_enqueue_style('slick');
wp_enqueue_style('slick');
wp_enqueue_style('fancybox');


//REGISTRAR JS
wp_register_script('fancybox', get_template_directory_uri() . '/inc/fancybox/jquery.fancybox.min.js', array(), '1.0.0', true);
wp_register_script('lazysizes', get_template_directory_uri() . '/js/lazysizes.min.js', array(), '5.1.1', true);
wp_register_script('slick', get_template_directory_uri() . '/inc/slick/slick.min.js', array(), '1.8.1', true);
wp_register_script('custom', get_template_directory_uri() . '/js/min.js', array(), '1.0', true);

//LLAMAR JS
wp_enqueue_script('fancybox');
wp_enqueue_script('lazysizes');
wp_enqueue_script('slick');
wp_enqueue_script('custom');

function imagen_destacada() {
	// Ruta de la imagen destacada (tamaño completo)
	// $imgDestacada['alt'] = get_post($thumbID)->post_excerpt;
	global $post;
	$thumbID = get_post_thumbnail_id( $post->ID );
	$imgDestacada['url'] = wp_get_attachment_url( $thumbID );
	$imgDestacada['title'] = get_post($thumbID)->post_title;
	$imgDestacada['alt'] = get_post_meta($thumbID,'_wp_attachment_image_alt', true);
	return $imgDestacada;
}

// ---------------------------------------------------- Optimizacion


//AÑADIR IMAGENES A POST DE BLOG
add_theme_support ('post-thumbnails');
///////////////////////////////////////////////////////////////////////////////////////////////////
// FUNCIONES PARA CREAR MENU DE OPCIONES DEL TEMA PERSONALIZADOS CON EL ADVANCED CUSTOM Field    //
///////////////////////////////////////////////////////////////////////////////////////////////////
// INCLUYE EL PHP CON LAS FUNCIONES QUE RECOGEN LOS DATOS DESDE EL PANEL DE WP
include "assets/opciones-generales.php";
// include "assets/contenido-flexible/bloque-default.php";
//////////////////////////////////////////////////////////////////////////////////////////
// Advanced Custom Field - Panel de Control personalizado

	if( function_exists('acf_add_options_page') )
	{
		acf_add_options_page(array(
			'page_title' 	=> 'Opciones Generales',
			'menu_title'	=> 'Opciones de Plantilla',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Datos Generales',
			'menu_title'	=> 'Datos Generales',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Configuración de Header',
			'menu_title'	=> 'Header',
			'parent_slug'	=> 'theme-general-settings',
    ));
    
    
		
	}
/////////////////////////////////////////////////////////////////////////////////////////
$postTags = get_the_tags();
if ( ! empty( $post_tags ) ) {
    echo '<ul>';
    foreach( $post_tags as $post_tag ) {
        echo '<li><a href="' . get_tag_link( $post_tag ) . '">' . $post_tag->name . '</a></li>';
    }
    echo '</ul>';
} 
// REGISTER QUE AÑADE LA OPCIÓN DE MENÚ AL WP
register_nav_menus (array (
	'menu_principal' => 'Menu Principal'
));
// add_shortcode('woosearch', 'get_product_search_form');
// add_filter('wp_nav_menu_items', 'rny_item_search', 10, 2);
// function rny_item_search($items, $args){
//   if($args->theme_location == 'menu_principal'){
//     $items .='<li class="menu-item item-search">'.get_product_search_form().'</li>';
//   }
//   return $items;
// }
///////////////////////////////////////////////////////////////////////////////////////7
// funcion QUE personaliza LA OPCIÓN leer más wn WP
function modify_read_more_link() {
    return '<div class="dato"><a class="more-link btn-read-more" href="'.get_permalink().'"></a></div>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );
///////////////////////////////////////////////////////////////////////////////////////7


/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return '[.....]';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function wpdocs_custom_excerpt_length( $length ) {
return 10;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

///////////////////////////////////////////////////////////////////////////////////////7
//registro de widget
// register_sidebar(
//   array(
//     'name' => 'Widgets Pag. Internas',
//     'id' => 'widget-default',
//     'description' => 'Área de widgets que aparece en las páginas internas por defecto',
//     'class' => '',
//     'before_widget' => '<div class="widget-default">',
//     'after_widget' => '</div>',
//     'before_title' => '<div class="tit">',
//     'after_title' => '</div>'
//   )
// );
register_sidebar(
  array(
    'name' => 'Widgets Footer 1',
    'id' => 'widget-footer-izquierda',
    'description' => 'Área de widgets que aparece en el footer 1',
    'class' => '',
    'before_widget' => '<div class="widget-footer-1">',
    'after_widget' => '</div>',
    'before_title' => '<div class="tit">',
    'after_title' => '</div>'
  )
);
register_sidebar(
  array(
    'name' => 'Widgets Footer 2',
    'id' => 'widget-footer-centro',
    'description' => 'Área de widgets que aparece en el footer 2',
    'class' => '',
    'before_widget' => '<div class="widget-footer-2">',
    'after_widget' => '</div>',
    'before_title' => '<div class="tit">',
    'after_title' => '</div>'
  )
);
register_sidebar(
  array(
    'name' => 'Widgets Footer 3',
    'id' => 'widget-footer-derecha',
    'description' => 'Área de widgets que aparece en el footer 3',
    'class' => '',
    'before_widget' => '<div class="widget-footer-3">',
    'after_widget' => '</div>',
    'before_title' => '<div class="tit">',
    'after_title' => '</div>'
  )
);

register_sidebar(
  array(
    'name' => 'Widgets Sub Footer',
    'id' => 'widget-sub-footer',
    'description' => 'Área de widgets que aparece en el Sub-Footer',
    'class' => '',
    'before_widget' => '<div class="widget-subfooter">',
    'after_widget' => '</div>',
    'before_title' => '<div class="tit">',
    'after_title' => '</div>'
  )
);
register_sidebar(
  array(
    'name' => 'Widgets Imágenes',
    'id' => 'widget-img',
    'description' => 'Área de widgets para colocar imagenes footer',
    'class' => '',
    'before_widget' => '<div class="widget-img">',
    'after_widget' => '</div>',
    'before_title' => '<div class="tit">',
    'after_title' => '</div>'
  )
);

register_sidebar(
  array(
    'name' => 'Widgets Tienda',
    'id' => 'widget-shop',
    'description' => 'Área de widgets para la Tienda',
    'class' => '',
    'before_widget' => '<div class="widget-shop">',
    'after_widget' => '</div>',
    'before_title' => '<div class="tit">',
    'after_title' => '</div>'
  )
);

////FUNCION DE ACF PARA RECOGER EL CONTENIDO FLEXIBLE DE LAS PÁGINAS //////////////


//funcion para poder subir archivos svg
// function add_file_types_to_uploads($file_types){
// $new_filetypes = array();
// $new_filetypes['svg'] = 'image/svg+xml';
// $file_types = array_merge($file_types, $new_filetypes );
// return $file_types;
// }
// add_action('upload_mimes', 'add_file_types_to_uploads');

/* Permitir subir archivos .svg */
function allow_svgimg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'allow_svgimg_types');

//permite subir archivos webp
function mi_nuevo_mime_type( $existing_mimes ) {
	// añade webp a la lista de mime types
	$existing_mimes['webp'] = 'image/webp';
	// devuelve el array a la funcion con el nuevo mime type
	return $existing_mimes;
}
add_filter( 'mime_types', 'mi_nuevo_mime_type' );

//eliminar versiones

function remove_scripts_version( $src ){
    $parts = explode( '?', $src );
    return $parts[0];
}
add_filter( 'script_loader_src', 'remove_scripts_version', 15, 1 );
add_filter( 'style_loader_src', 'remove_scripts_version', 15, 1 );

//añadir preload a script

function add_preload_rel($tag, $handle) {
  $styles_to_preload = array('contact-form-7','wp-block-library','cookie-law-info','cookie-law-info-gdpr','rs-plugin-settings','fontawesome');
  foreach($styles_to_preload as $preload_style) {
    if ($preload_style === $handle) {
      return str_replace("rel='stylesheet'", "rel='stylesheet preload' as='style'", $tag);
    }
  }
  return $tag;
}
add_filter('style_loader_tag', 'add_preload_rel', 10, 2);

//Añadir defer para scripts

function add_defer_attribute($tag, $handle) {
  $scripts_to_defer = array('jquery-migrate','wpcf7-ga-events','contact-form-7','cookie-law-info','tp-tools','revmin','wp-embed','google-recaptcha','custom','fancybox');
  foreach($scripts_to_defer as $defer_script) {
    if ($defer_script === $handle) {
      return str_replace(' src', ' defer="defer" src', $tag);
    }
  }
  return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



?>