<?php
get_header();
?>
<main class="default pag-interna">
<?php if(have_posts()){
    while(have_posts()){
      the_post();
//sección banner principal
?>
      
<?php 
  the_content(); 
?>
   
<?php  } //cierra while principal
  }else{?>
  <section class="banner-interna">
        <img class="lazyload" data-src="/wp-content/uploads/2020/07/fondo-marisco.jpg" >
        <div class="inner"></div>
        <div class="titulo">
          <span class="title">
          <?php 
            the_title();
           ?>
           404<br> PÁGINA NO ENCONTRADA
          </span>
        </div>
      </section> 
      
    <div class="breadcrumb"><?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?></div>

    <div class="contenedor">
     <section class="content default">
      <div class="tit" style="text-align:center;">
        <p>Lo sentimos, página no encontrada.</p>
      </div>
      <div><p>Volver a <a href="<?php echo home_url(); ?>">Inicio</a></p></div>
      <div class="no-found">
        <div class="error-message">404</div>
      </div>
  </section>
  <?php
  } //cierra if principal
?>
</div>
</main>
<?php
get_footer();
?>
