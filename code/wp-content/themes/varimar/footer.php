    <footer>
      <section class="footer">
        <div class="fondo">
          <div class="contenedor">
            <div class="bloques">
              <div class="bloque widget-izquierda">
              <?php  if(is_active_sidebar('widget-footer-izquierda')){
                  dynamic_sidebar('widget-footer-izquierda');
                }
              ?>
              </div>
              <div class="bloque widget-centro">
              <?php  if(is_active_sidebar('widget-footer-centro')){
                dynamic_sidebar('widget-footer-centro');
                }
              ?>
              <div class="rrss">
                <?php 
                  $claseFacebook = "fab fa-facebook-f" ;
                  $claseTwitter ="fab fa-twitter";
                  $claseLinkedin ="fab fa-linkedin-in";
                  $claseInstagram = "fab fa-instagram";
                  $claseYoutube="fab fa-youtube";
                  if(carbon_get_theme_option('crb_facebook_link')){?>
                    <a href="<?php echo carbon_get_theme_option('crb_facebook_link');?>" target="_blank" rel="nofollow noopener" title="facebook"><i class="<?php echo $claseFacebook;?>"></i></a>
                  <?php }
                  if(carbon_get_theme_option('crb_twitter_link')){?>
                    <a href="<?php echo carbon_get_theme_option('crb_facebook_link');?>" target="_blank" rel="nofollow noopener" title="Twitter"><i class="<?php echo $claseTwitter;?>"></i></a>
                  <?php }
                  if(carbon_get_theme_option('crb_linkedin_link')){?>
                    <a href="<?php echo carbon_get_theme_option('crb_linkedin_link');?>" target="_blank" rel="nofollow noopener" title="Linkedin"><i class="<?php echo $claseLinkedin;?>"></i></a>
                  <?php }
                  if(carbon_get_theme_option('crb_instagram_link')){?>
                    <a href="<?php echo carbon_get_theme_option('crb_instagram_link');?>" target="_blank" rel="nofollow noopener" title="Instagram"><i class="<?php echo $claseInstagram;?>"></i></a>
                  <?php }
                  if(carbon_get_theme_option('crb_youtube_link')){?>
                    <a href="<?php echo carbon_get_theme_option('crb_youtube_link');?>" target="_blank" rel="nofollow noopener" title="Youtube"><i class="<?php echo $claseYoutube;?>"></i></a>
                  <?php }
                  ?>
              </div>

              </div>
              <div class="bloque widget-derecha">
              <?php  if(is_active_sidebar('widget-footer-derecha')){
                  dynamic_sidebar('widget-footer-derecha');
                }
              ?>
              </div>
            <div class="bloque widget-imagenes">
              <?php  if(is_active_sidebar('widget-img')){
                dynamic_sidebar('widget-img');
              }
              ?>
              </div>
            
              </div>
          </div>
        </div>
      </section>
      <section class="sub-footer">
        <div class="contenedor">
      <?php
        if(is_active_sidebar('widget-sub-footer')){
          dynamic_sidebar('widget-sub-footer');
        }
        echo "</div>";
      echo "</section>";
    ?>
    <?php wp_footer(); ?>
    </footer>
  </body>
  <script>
  window.onscroll = function() {myFunction()};
  var navbar = document.getElementById("main-head");
  var logo = document.getElementById("logo");
  var sticky = navbar.offsetTop;
  function myFunction(){
    if(window.pageYOffset >= sticky){
      navbar.classList.add("sticky-nav");
      logo.classList.add("sticky");
    } else {
      navbar.classList.remove("sticky-nav");
      logo.classList.remove("sticky");
    }
  }
  </script>
</html>