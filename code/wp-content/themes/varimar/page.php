<?php
get_header();
add_thickbox();
?>
<main class="pag-interna tienda" >
<?php //sección banner principal
      $banner = carbon_get_the_post_meta('crb_banner');
      $text_banner = carbon_get_the_post_meta('texto-banner');
    if($banner){?>
      <section class="banner-interna">
        <img class="lazyload" data-src="<?php echo $banner; ?>" alt="<?php echo $banner['alt']; ?>" title="<?php echo $banner['title']; ?>">
        <div class="inner"></div>
        <div class="titulo">
          <span class="title">
          <?php if($text_banner){
            echo $text_banner;
          }else{
            the_title();
          }
           ?>
          </span>
        </div>
   <?php }else{?>
      <section class="banner-interna">
        <img src="/wp-content/uploads/2020/07/fondo-marisco.jpg">
        <div class="inner"></div>  
        <div class="titulo">
          <span class="title">
          <?php if($text_banner){
            echo $text_banner;
          }else{
            the_title();
          }
           ?>
          </span>
        </div>
    <?php }//fin seccion banner principal
  ?>
    </section> 
    <div class="breadcrumb"><?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?></div>

<?php //BODY---------------------

?>
  <?php if(have_posts()){
    while( have_posts()){
      the_post(); 
  //ABRE CONTENEDOR PRINCIPAL-----------------------------------------------------------------------------------    ?>
      <div class='contenedor'>
       <?php the_content(); ?>
      </div>
       

  <?php }//cierra while principal
  }else{ ?>
  <section class="titulo-seccion titulo-default">
    <div class="bloque-texto">
      <div class="titulo">
       Lo sentimos, página no encontrada.
      </div>
      <div class="linea"><hr></div>
    </div>  
      <img src="/404.jpg">
  </section>
  <?php } ?>
  </div>
</main><!--cierra section main default -->
<?php
get_footer();
?>
