<?php
/*
Template Name: Página Interna
*/
get_header();
?>
<?php if(get_field('id_pagina')){
  $id= get_field('id_pagina');?>
  <main id="<?php echo $id;?>" class="pag-interna">
<?php }else{?>
<main class="pag-interna">
<?php } ?>

  <?php if(have_posts()){
    while(have_posts()){
      the_post();
      the_content(); ?>
  
    <section class="banner-interna">
    <div class="inner"></div>
      <?php if(get_field('banner')){?>
          <?php $banner = get_field('banner'); ?>
          <img class="lazyload" data-src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>" title="<?php echo $banner['title']; ?>">
          <div class="titulo">
            <span class="title"><?php the_title(); ?></span>
          </div>
      <?php }else{?>
          <img class="lazyload" src="/wp-content/uploads/2020/03/banner-tratamientos.jpg" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
            <div class="titulo">
              <span class="title"><?php the_title(); ?></span>
            </div>
          <?php }?> 
    </section> 
    <div class="breadcrumb"><?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?></div>

<section class="contenido">
<?php if (have_rows('secciones_internas')){
    while(have_rows('secciones_internas')): the_row();

    /* ----------------------------------------------------------------------------------------------------------------------------- */
        //bloque texto + imagen
          if(get_row_layout() == 'texto_imagen'){
            $image = get_sub_field('imagen');
            $id = get_sub_field('nombre_seccion');
            if(get_sub_field('orden') == 'Texto Primero'){
              $orden = "text-img";
            }else{
              $orden = "img-text";
            }
            
            $fondo= get_sub_field('fondo_gris');
            if($id){?>
              <section id="<?php echo $id;?>" class="texto-imagen">
            <?php }else{?>
              <section class="texto-imagen <?php echo $fondo;?>">
            <?php }?>
              <div class="content">
                <div class="row <?php echo $orden; ?>">
                  <?php if($image){?>
                    <div class="imagen">
                      <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">
                    </div>
                  <?php } ?>
                  <?php if(get_sub_field('texto')){?>
                    <div class="texto">
                    <?php if(get_sub_field('titulo')){?>
                      <<?php the_sub_field('etiqueta_seo');?> class="titulo">
                      <?php the_sub_field('titulo'); ?>
                      </<?php the_sub_field('etiqueta_seo');?>>
                    <?php } ?>
                      <?php the_sub_field('texto'); ?>
                      <?php if(get_sub_field('enlace')){?>
                        <div class="boton">
                          <a href="<?php the_sub_field('enlace'); ?>"><?php the_sub_field('texto_boton');?></a>
                        </div>
                      <?php }?>
                    </div>
                <?php } ?>
                </div>
              </div>
            </section>
        <?php }//cierra bloque texto+imagen
    /* ----------------------------------------------------------------------------------------------------------------------------- */
      //bloque solo texto
        if(get_row_layout() == 'solo_texto'){
          $fondo= get_sub_field('fondo_gris');
          $id = get_sub_field('nombre_seccion');
          if($id){?>
            <section id="<?php echo $id;?>" class="bloque-solo-text <?php echo $fondo;?>">
          <?php }else{?>
            <section class="bloque-solo-text <?php echo $fondo;?>">
          <?php }?>
              <div class="content">
                <div class="row">
                  <?php if(get_sub_field('titulo')){?>
                    <<?php the_sub_field('etiqueta_seo'); ?> class="titulo">
                      <?php the_sub_field('titulo'); ?>
                    </<?php the_sub_field('etiqueta_seo'); ?>>
                  <?php } //titulo ?>
                  <?php if(get_sub_field('dos_columnas')){?>
                    <div class="texto dos_column">
                      <?php the_sub_field('contenido'); ?>
                    </div>
                  <?php }else{?>
                    <div class="texto">
                      <?php the_sub_field('contenido'); ?>
                    </div>
                  <?php } // texto dos columnas ?>
                </div>
              </div>
            </section>
        <?php }//cierra bloque solo-texto
  
/* ----------------------------------------------------------------------------------------------------------------------------- */
      //bloque formulario generico
      if(get_row_layout() == 'bloque_formulario_generico'){?>
        <section class="form-generico">
          <div class="row">
            <?php if(get_sub_field('anadir_formulario_generico')){
              $existeformulario = get_field('formulario_generico', 'option');
              if($existeformulario){
                echo do_shortcode($existeformulario);
              }
            }?>
          </div>
        </section>
      <?php } //cierra bloque formulario generico 

  /* ----------------------------------------------------------------------------------------------------------------------------- */
      //bloque imagen
      if(get_row_layout() == 'imagen_sola') { ?>
        <?php if(get_sub_field('nombre_seccion')){
          $id= get_sub_field('nombre_seccion');
        } //nombre seccion ?>
        <section id="<?php echo $id; ?>" class="only-img">
          <div class="row">
          <?php if(get_sub_field('imagen')){
            $imagen = get_sub_field('imagen');
            ?>
            <img class="lazyload" data-src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>" title="<?php echo $imagen['title']; ?>">
                    
          <?php } //imagen  ?>
          </div>
        </section>
      <?php }   //cierra bloque imagen sola 

/* ----------------------------------------------------------------------------------------------------------------------------- */
      //bloque acordeón 
      if(get_row_layout() == 'bloques_acordeon'){
        $id = get_sub_field('nombre_seccion');
        $fondo= get_sub_field('fondo_gris');
        ?>
        <section id="<?php echo $id;?>" class="acordeon <?php echo $fondo;?>">
          <div class="row">
            <?php if(get_sub_field('titulo')){?>
              <<?php the_sub_field('etiqueta_seo'); ?> class="titulo">
                <?php the_sub_field('titulo');?>
              </<?php the_sub_field('etiqueta_seo'); ?>>
            <?php } ?>
            <?php if(get_sub_field('texto')){
                if(get_sub_field('dos_columnas')){?>
                    <div class="texto dos_column">
                      <?php the_sub_field('texto'); ?>
                    </div>
                  <?php }else{?>
                    <div class="texto">
                      <?php the_sub_field('texto'); ?>
                    </div>
                  <?php } // texto dos columnas
              } //texto ?>
            <?php if(have_rows('bloque')){
              $cont=1;
              ?>
              <div class="acordeon-content">
                <ul>
                  <?php 
                  while(have_rows('bloque')): the_row();?>
                    <li>
                      <input type="checkbox" checked>
                      <i></i>
                      <span class="titulo">
                        <?php the_sub_field('titulo_bloque');?>
                      </span>
                      <div class="contenido">
                      <div class="texto">
                        <?php the_sub_field('contenido'); ?>
                      </div>
                      </div>                      
                  <?php $cont++;
                  endwhile;?>
                </ul>
              </div>
            <?php }?>
          </div>
        </section>
      <?php } //cierra bloque acordeón
/* ----------------------------------------------------------------------------------------------------------------------------- */
    //bloque opiniones 
    if(get_row_layout() == 'bloque_opiniones') {?>
    
    <section class="opiniones">
      <div class="row">
      <div class="bloque-opiniones">
      <?php if(have_rows('opinion')){
        $cont=1;?>
        <?php while(have_rows('opinion')): the_row();?>
       
          <div class="opinion opinion-<?php echo $cont;?>">
          <?php if(get_sub_field('nombre')){?>
            <div class="nombre">
              <?php the_sub_field('nombre'); ?>
            </div>
          <?php }
          if(get_sub_field('texto')){?>
            <div class="text-opinion">
              <?php the_sub_field('texto'); ?>
            </div>
          <?php }?>
       </div>
       <?php $cont++;
        endwhile; ?>
        <?php  ?>
      <?php }?>
      </div>
      </div>
    </section>
   <?php  }//cierra bloque opiniones


  ?>
  <?php endwhile; //cierra while secciones internas
  } //cierra if secciones internas ?>
  </section>
<?php
    } //cierra while principal ?>

<?php
  }else{
    echo 'Lo siento, la página no ha sido encontrada';
  } //cierra if principal
?>
</main>
<?php
get_footer();
?>
