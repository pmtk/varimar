<?php
get_header();?>
 <?php $objetoactual= get_queried_object();
  if($objetoactual){?>
    <main id="pag-blog" class="post-blog">
      <section class="banner-interna">
        <div class="inner"></div>
        <?php if(get_field('banner', $objetoactual)){?>
          <?php $banner = get_field('banner', $objetoactual); ?>
          <img src="<?php echo $banner['url']; ?>" title="<?php echo $banner['title']; ?>" alt="<?php echo $banner['alt']; ?>">
          <div class="titulo">
            <span class="title"><?php //echo $objetoactual->name;?>Noticias</span>
          </div>
        <?php }else{?>
          <img class="lazyload" src="/wp-content/uploads/2020/03/banner-tratamientos.jpg" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
          <div class="titulo">
            <span class="title">Noticias</span>
          </div>
        <?php }?>
      </section>
  

    <div class="contenido">  
      <?php $category_id = get_queried_object_id();
      $cat_id = 'category_'.$category_id; ?>
      <div class="content">
        <div class='bloque-entradas'>
          <?php if(have_posts()){
            while( have_posts()){
              the_post();?>
              <div class='entrada taphover'>
                <a href="<?php echo get_permalink(); ?>">
                  <div class="imagen">
                    <span class="inner"></span>
                    <div class="ico"><img src="/wp-content/uploads/2020/03/zoom-in.png" title="saber más" alt="saber más"></div>
                    <?php the_post_thumbnail();?>
                  </div>
                  <div class="text-content">
                    <div class="tit">
                      <?php 
                      $g_name = get_the_title();
                      echo substr($g_name,0,25).' ...';
                      ?>
                    </div>
                  </div>
                </a>
              </div>
            <?php } ?>
        </div>
          <div class="pagination">
            <?php previous_posts_link();
            next_posts_link(); ?>
          </div>
        <div class='bloque-widget'>
          <?php if(is_active_sidebar('widget-blog')){
            dynamic_sidebar('widget-blog');
          }?>
        </div>
      </div>
    <?php }else{
      echo "No hay entradas de Blog";
    } ?>
  </div>
</main>
<?php  }?>
<?php
get_footer();
?>


