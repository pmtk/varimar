<?php
get_header();
?>
<div class="banner-interna">
  <?php if(get_field('banner')){?>
    <img src="<?php the_field('banner'); ?>">
    <div class="titulo">
      <span class="title"><?php the_title(); ?></span>
    </div>
  <?php }else{?>
    <img src="/wp-content/uploads/2020/03/banner-tratamientos.jpg" alt="<?php echo bloginfo('name'); ?>" title="<?php echo bloginfo('name'); ?>">
    
  <?php }?>
</div>
<section class="content default">
  <div class="contenedor">
  
  <?php
    if(have_posts())
    {
      while( have_posts() )
      {
        the_post();
       
        
        the_content();
      }
    }
    else
    {?>
    
    <section class="titulo-seccion titulo-default">
    <div class="bloque-texto">
      <div class="titulo" style="text-align:center;">
       Lo sentimos, página no encontrada.
      </div>
     
    </div>  
      <div class="no-found"><div class="error-message">404</div></div>
  </section>
     

      <?php
    }
   ?>
 </div>
</section>
<?php
get_footer();
?>
