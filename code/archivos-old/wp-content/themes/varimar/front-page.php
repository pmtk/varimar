<?php
/*
Template Name: Inicio
*/
get_header();
//BODY ////////////////////////////////////////////
?>
<main class="main">
  <div class="contenedor">
  <?php
    if(have_posts())
    {
      while( have_posts() )
      {
        the_post();
        the_content();
//bloques secciones contenido flexible

       
      ?>
      <div class="aqui">
       <?php 
       $bloq_iconos = carbon_get_the_post_meta('crb_iconos');
       if($bloq_iconos){?>
        <div class="bloque-iconos">
        <?php foreach($bloq_iconos as $bloque_icono){?>
          <div class="icono">
            <div class="ico-img">
               <img src="<?php echo $bloque_icono['image'];  ?>">
            </div>
            <div class="ico-tit">
              <?php echo $bloque_icono['title']; ?>
            </div>
            <div class="ico-text">
               <?php echo $bloque_icono['descripcion']; ?>
            </div>
          </div>
        <?php }
        ?>
        </div>
      <?php }
        ?>
        
      </div>
    <?php }//cierra while principal
  }else{
    echo 'Lo siento, la página no ha sido encontrada';
  }//cierra if principal
   ?>
   </div>
 </main>
<?php
get_footer();
?>
<script>
$('div.taphover').on('touchstart', function (e) {
    'use strict'; //satisfy code inspectors
    var link = $(this); //preselect the link
    if (link.hasClass('hover')) {
        return true;
    } else {
        link.addClass('hover');
        $('div.taphover').not(this).removeClass('hover');
        e.preventDefault();
        return false; //extra, and to make sure the function has consistent return points
    }
});
</script>
<script>
jQuery(document).ready(function(){
  jQuery('.destacados>.wp-block-group__inner-container>.wc-block-grid>.wc-block-grid__products').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      centerMode: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 770,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
  });

});
</script>