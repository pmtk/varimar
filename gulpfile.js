//--------------------------------------------------------------------------------------------------------------------------------------------------
//  
//  Version 1.1.0
//  
//--------------------------------------------------------------------------------------------------------------------------------------------------
//  
//  Limpiar cache 
//  npm cacheclean --force
//
//  Instalar paquetes de forma global  **(ver npm link)
//  npm install --global <nombre-paquete>
//  npm install --global gulp dotenv browser-sync node-sass gulp-sass gulp-cssmin gulp-concat gulp-uglify mysqldump gulp-replace gulp-rename prompts del gulp-sourcemaps
//  
//  Crear Enlaces al proyecto de los módulos instalados de forma global **(Si no están instalados los instala automaticamente)
//  npm link <nombre-paquete>
//  npm link gulp dotenv browser-sync node-sass gulp-sass gulp-cssmin gulp-concat gulp-uglify mysqldump gulp-replace gulp-rename prompts del gulp-sourcemaps
//  
//  
//--------------------------------------------------------------------------------------------------------------------------------------------------


/*
 * Dependencias
 */
var gulp = require('gulp');
const { series, paralel } = gulp;
require('dotenv').config();
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var mysqlDump = require('mysqldump');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
const prompts = require('prompts');
var del = require('del');
var sourcemaps = require('gulp-sourcemaps');


/**
 * Variable para el return de las funciones
 * con esto se marca el fin de la tarea
 */
var fin = new Promise(function(resolve, reject) { resolve(); });


/**
 * Esta variable, define, sobre que BBDD se va a trabajar, 
 * Se modifica mediante una pregunta al usuario, y su valor, 
 * determina el funcionamiento del resto de tareas
 * 
 * 1 - Base de datos del repositorio
 * 2 - Base de datos de Pre-producción
 * 3 - Base de datos de Producción
 */
var bbdd = 0;
var bbdd_aux = 0;

/*
 * Generamos una variable auxiliar para determinar si estamos en proceso de despliegue,
 * Esto influye en las operaciones de cambio de url de BBDD
 */
var despliegueBD = 0;


/**
 * Creación de argumentos introducidos en linea de comandos
 * Para su uso en las funciones
 */
const arg = (argList => {

    let arg = {},
        a, opt, thisOpt, curOpt;
    for (a = 0; a < argList.length; a++) {

        thisOpt = argList[a].trim();
        opt = thisOpt.replace(/^\-+/, '');

        if (opt === thisOpt) {

            /* argument value */
            if (curOpt) arg[curOpt] = opt;
            curOpt = null;

        } else {

            /* argument name */
            curOpt = opt;
            arg[curOpt] = true;
        }
    }
    return arg;
})(process.argv);



function carga() {
   
    return fin;
}
exports.carga = carga;

/*
 * Configuración de la tarea por defecto
 */
function start() {
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    console.log('\x1b[34m%s\x1b[0m', '| Iniciando Gulp             |');
    console.log('\x1b[34m%s\x1b[0m', '|----------------------------|');

    if (process.env.SASS == 1) {
        console.log('\x1b[32m%s\x1b[0m', '| - Compilador SASS          |');
        gulp.watch(process.env.THEMETHEMECODE + process.env.SASS_FOLDER + '/**/*.sass', gulp.series('sass'));
    }

    if (process.env.SCSS == 1) {
        console.log('\x1b[32m%s\x1b[0m', '| - Compilador SCSS          |');
        gulp.watch(process.env.THEMETHEMECODE + process.env.SCSS_FOLDER + '/**/*.scss', gulp.series('scss'));
    }

    if (process.env.CSS_MIN == 1) {
        console.log('\x1b[32m%s\x1b[0m', '| - Minificado de CSS        |');
        gulp.watch(process.env.THEMETHEMECODE + process.env.CSS_FOLDER + '/**/*.css', gulp.series('compactacss'));
    }

    if (process.env.JS_MIN == 1) {
        console.log('\x1b[32m%s\x1b[0m', '| - Minificado js            |');
        gulp.watch(process.env.THEMETHEMECODE + process.env.JS_FOLDER + '/**/*.js', gulp.series('compactajs'));
    }

    if (process.env.BROWSER_SYNC == 1) {
        console.log('\x1b[32m%s\x1b[0m', '| - Navegador en Tiempo real |');
        //En caso de activar la apertura del navegador
        if (process.env.ABRIR_NAVEGADOR_AUTO == 1) {
            browserSync.init({
                proxy: process.env.PROXY,
                // ghostMode: false,
                https: true
            });
        } else {
            browserSync.init({
                proxy: process.env.PROXY,
                // ghostMode: false,
                https: true,
                open: false
            });
        }

        //HTML
        gulp.watch(process.env.THEMETHEMECODE + '/**/*.html').on('change', function() {
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            console.log('\x1b[32m%s\x1b[0m', '|  Detectado cambio de HTML  |');
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            browserSync.reload();
        });

        //PHP
        gulp.watch(process.env.THEMETHEMECODE + '/**/*.php').on('change', function() {
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            console.log('\x1b[32m%s\x1b[0m', '|  Detectado cambio de PHP   |');
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            browserSync.reload();
        });

        //CSS
        gulp.watch(process.env.THEMETHEMECODE + process.env.CSS_FOLDER_MIN + process.env.CSS_FILE_MIN).on('change', function() {
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            console.log('\x1b[32m%s\x1b[0m', '|  Detectado cambio de CSS   |');
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            browserSync.reload();
        });

        //JS
        gulp.watch(process.env.THEMETHEMECODE + process.env.JS_FOLDER_MIN + process.env.JS_FILE_MIN).on('change', function() {
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            console.log('\x1b[32m%s\x1b[0m', '|   Detectado cambio de JS   |');
            console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
            browserSync.reload();
        });
    }
    console.log('\x1b[32m%s\x1b[0m', '+----------------------------+');
}
exports.start = start;

/*
 * Configuración de la tarea 'SASS'
 */
function compilasass() {
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    console.log('\x1b[34m%s\x1b[0m', '|       Compilando SASS      |');
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    return gulp.src(process.env.THEMETHEMECODE + process.env.SASS_FOLDER + '/**/*.sass')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(rename({ suffix: '.min' })) 
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(process.env.THEMETHEMECODE + process.env.CSS_FOLDER));
}
exports.sass = compilasass;

/*
 * Configuración de la tarea 'SCSS'
 */
function compilascss() {
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    console.log('\x1b[34m%s\x1b[0m', '|       Compilando SCSS      |');
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    return gulp.src(process.env.THEMETHEMECODE + process.env.SCSS_FOLDER + '/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(process.env.THEMETHEMECODE + process.env.CSS_FOLDER));
}
exports.scss = compilascss;

/*
 * Configuración de la tarea 'compactacss'
 */
function compactacss() {
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    console.log('\x1b[34m%s\x1b[0m', '|       Compactando CSS      |');
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    console.log('\x1b[34m%s\x1b[0m', process.env.THEMETHEMECODE + process.env.CSS_FOLDER + '/**/*.css');
    return gulp.src(process.env.THEMETHEMECODE + process.env.CSS_FOLDER + '/**/*.css')
        .pipe(concat(process.env.CSS_FILE_MIN))
        .pipe(cssmin())
        .pipe(gulp.dest(process.env.THEMETHEMECODE + process.env.CSS_FOLDER_MIN));
}
exports.compactacss = compactacss;
/*
 * Configuración de la tarea 'compactajs'
 */

function compactajs() {
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    console.log('\x1b[34m%s\x1b[0m', '|       Compactando JS       |');
    console.log('\x1b[34m%s\x1b[0m', '+----------------------------+');
    return gulp.src(process.env.THEMETHEMECODE + process.env.JS_FOLDER + '/**/*.js')
        .pipe(concat(process.env.JS_FILE_MIN))
        .pipe(uglify())
        .pipe(gulp.dest(process.env.THEMETHEMECODE + process.env.JS_FOLDER_MIN))
}
exports.compactajs = compactajs;

/*
 * Configuración de la tarea 'dockerup'
 */
function dockerup() {

    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[33m%s\x1b[0m', '|     Iniciando Docker      |');
    console.log('\x1b[33m%s\x1b[0m', '| - docker-compose up -d -  |');
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');

    var exec = require('child_process').exec,
        child;
    child = exec('docker-compose up -d');

    return child;
}
exports.dockerup = dockerup;

/*
 * Configuración de la tarea 'dockerdown'
 */
function dockerdown() {
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[33m%s\x1b[0m', '|      Parando Docker       |');
    console.log('\x1b[33m%s\x1b[0m', '|  - docker-compose down -  |');
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');

    var exec = require('child_process').exec,
        child;

    return child = exec('docker-compose down',
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}
exports.dockerdown = dockerdown;

/*
 * Configuración de la tarea 'gitpull'
 */
function gitpull() {
    // var comando = 'git -c http.sslVerify=false pull ' + process.env.URLGIT;
    var comando = 'git pull ' + process.env.URLGIT;
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[33m%s\x1b[0m', '|     Git haciendo Pull     |');
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[32m%s\x1b[0m', comando);
    console.log('');

    var exec = require('child_process').exec,
        child;

    return child = exec(comando,
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}
exports.gitpull = gitpull;

/*
 * Configuración de la tarea 'gitadd'
 */

function gitadd() {
    var comando = 'git add -A';
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[33m%s\x1b[0m', '|     Git haciendo add      |');
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[32m%s\x1b[0m', comando);
    console.log('');

    var exec = require('child_process').exec,
        child;

    return child = exec(comando,
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}
exports.gitadd = gitadd;


/*
 * Configuración de la tarea 'gitcommit'
 */
function gitcommit() {

    var date = new Date();
    var newDate = date.toLocaleString()
    var comando = 'git commit -m"' + newDate + '"';
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[33m%s\x1b[0m', '|    Git haciendo Commit    |');
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[32m%s\x1b[0m', comando);
    console.log('');

    var exec = require('child_process').exec,
        child;

    return child = exec(comando,
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}
exports.gitcommit = gitcommit;

/*
 * Configuración de la tarea 'gitpush'
 */
function gitpush() {
    // var comando = 'git -c http.sslVerify=false push -u ' + process.env.URLGIT;
    var comando = 'git push ' + process.env.URLGIT;
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[33m%s\x1b[0m', '|     Git haciendo Push     |');
    console.log('\x1b[33m%s\x1b[0m', '+---------------------------+');
    console.log('\x1b[32m%s\x1b[0m', comando);
    console.log('');

    var exec = require('child_process').exec,
        child;

    return child = exec(comando,
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}
exports.gitpush = gitpush;

/*
 * Cambiar URL en BBDD 
 */
function bbddCambioUrl() {

    /**
     * Inicializamos las variables necesarias para la ejecución del comando
     */

    var buscar = '';
    var sustituir = '';

    /* Variable auxiliar para evitar mostrar el mensaje de error mas de una vez */
    var error = 0;



    /**
     * En caso de que nos encontremos en un despliegue 
     * en el que NO queremos tocar la BBDD
     * Marcamos error para evitar sustituir las url
     */
    if (despliegueBD == 'No') {
        error = true;
    }

    /**
     * Evaluamos los argumentos recibidos
     */
    if (arg.url1 != '' && arg.url2 != '') {
        if (arg.url1 != arg.url2) {
            /* Evaluamos el argumento --url1 */
            switch (arg.url1) {
                case 'dev':
                    /* Asignamos la url de desarrollo */
                    buscar = 'http://localhost'
                    break;
                case 'preprod':
                    /* Asignamos la url de pre-produccion */
                    buscar = process.env.PROTOCOLO_PREPROD + process.env.DOMINIO_PREPROD
                    break;
                case 'prod':
                    /* Asignamos la url de produccion */
                    buscar = process.env.PROTOCOLO_PROD + process.env.DOMINIO_PROD
                    break;
                default:
                    error = true;
                    break;
            }

            /* Evaluamos el argumento --url2 */
            switch (arg.url2) {
                case 'dev':
                    /* Asignamos la url de desarrollo */
                    sustituir = 'http://localhost'
                    break;
                case 'preprod':
                    /* Asignamos la url de pre-produccion */
                    sustituir = process.env.PROTOCOLO_PREPROD + process.env.DOMINIO_PREPROD
                    break;
                case 'prod':
                    /* Asignamos la url de produccion */
                    sustituir = process.env.PROTOCOLO_PROD + process.env.DOMINIO_PROD
                    break;
                default:
                    error = true;
                    break;
            }
        } else {
            /* Argumentos idénticos */
            error = true;
        }
    } else {
        /* Argumentos vacíos */
        error = true;
    }


    /* Evaluamos si todo está correcto o por el contrario debemos mostrar el error */
    if (error) {

        /**
         * Si se ha encontrado un error, mostramos la documentación de la función,
         * salvo en el caso en el que se ha llamado a la funcion mediante un script
         * y la url de destino y fin son la de desarrollo, en ese caso, implica que 
         * no se debe modificar la BBDD, pero tampoco se tiene porque mostrar un error
         */

        if ((bbdd != 0 && arg.url1 == 'dev' && arg.url2 == 'dev') || despliegueBD == 'No') {
            console.info('\x1b[32m%s\x1b[0m', '+---------------------------------------+');
            console.info('\x1b[32m%s\x1b[0m', '|  No ha sido necesario cambiar la URL  |');
            console.info('\x1b[32m%s\x1b[0m', '+---------------------------------------+');
            return fin;
        } else {
            console.error('\x1b[31m%s\x1b[40m', '+--------------------------------------------------------------------------------------------+');
            console.error('\x1b[31m%s\x1b[40m', '|  Argumento inválido, use uno de los siguientes argumentos                                  |');
            console.error('\x1b[31m%s\x1b[40m', '+--------------------------------------------------------------------------------------------+');
            console.error('\x1b[31m%s\x1b[40m', '|  Argumento --url1 (Indica la url que queremos sustituir)                                   |');
            console.error('\x1b[31m%s\x1b[40m', '+--------------------------------------------------------------------------------------------+');
            console.error('\x1b[31m%s\x1b[40m', '|  --url1 dev // localhost                                                                   |');
            console.error('\x1b[31m%s\x1b[40m', '|  --url1 preprod // Utiliza la url (protocolo + dominio) pre-producción indicado en el .env |');
            console.error('\x1b[31m%s\x1b[40m', '|  --url1 prod // Utiliza la url (protocolo + dominio) producción indicado en el .env        |');
            console.error('\x1b[31m%s\x1b[40m', '+--------------------------------------------------------------------------------------------+');
            console.error('\x1b[31m%s\x1b[40m', '|  Argumento --url2 (Indica la url que queremos que sustituya a la url1)                     |');
            console.error('\x1b[31m%s\x1b[40m', '+--------------------------------------------------------------------------------------------+');
            console.error('\x1b[31m%s\x1b[40m', '|  --url2 dev // localhost                                                                   |');
            console.error('\x1b[31m%s\x1b[40m', '|  --url2 preprod // Utiliza la url (protocolo + dominio) pre-producción indicado en el .env |');
            console.error('\x1b[31m%s\x1b[40m', '|  --url2 prod // Utiliza la url (protocolo + dominio) producción indicado en el .env        |');
            console.error('\x1b[31m%s\x1b[40m', '+--------------------------------------------------------------------------------------------+');
        }

    } else {



        /* Si no hay error ejecutamos el comando */
        console.log('+------------------------------+');
        console.log('|  Reemplazando urls en BBDD   |');
        console.log('+------------------------------+');
        console.log('');
        console.log('docker-compose run --rm cli search-replace "' + buscar + '" "' + sustituir + '" --all-tables --report-changed-only');
        console.log('');

        var exec = require('child_process').exec,
            child;
        return child = exec('docker-compose run --rm cli search-replace "' + buscar + '" "' + sustituir + '" --all-tables --report-changed-only',
            function(error, stdout, stderr) {
                console.log(stdout);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
    }
}
exports.bbddCambioUrl = bbddCambioUrl;

/*
 * Configuración de la tarea para exportar la BBDD
 */
function dumpDatabase() {
    /*Evaluamos la variable auxiliar que nos indica sobre que BBDD trabajar*/
    if (bbdd == 0) {
        /**
         * La variable se inicia a 0, por lo que si este valor
         * no se ha modificado, buscamos si han pasado argumentos 
         * indicando la acción
         */
        switch (arg.bbdd) {
            case 'dev':
                bbdd = '1';
                break;
            case 'preprod':
                bbdd = '2';
                break;
            case 'prod':
                bbdd = '3';
                break;
            default:
                console.error('\x1b[31m%s\x1b[0m', '+-----------------------------------------------------------------------------+');
                console.error('\x1b[31m%s\x1b[0m', '|  Argumento inválido, use uno de los siguientes argumentos                   |');
                console.error('\x1b[31m%s\x1b[0m', '+-----------------------------------------------------------------------------+');
                console.error('\x1b[31m%s\x1b[0m', '|  --bbdd dev // Realiza un dump de la base de datos local                    |');
                console.error('\x1b[31m%s\x1b[0m', '|  --bbdd preprod // Realiza un dump de la base de datos de pre-producción    |');
                console.error('\x1b[31m%s\x1b[0m', '|  --bbdd prod // Realiza un dump de la base de datos de producción           |');
                console.error('\x1b[31m%s\x1b[0m', '+-----------------------------------------------------------------------------+');
                break

        }
    }

    /**
     * Evaluamos el valor de la variable bbdd
     * Este valor nos indicará sobre que bbdd
     * hacer el dump
     * 1 - Base de datos del repositorio
     * 2 - Base de datos de Pre - producción
     * 3 - Base de datos de Producción
     */
    var dump = '';
    var host = '';
    var user = '';
    var password = '';
    var database = '';

    switch (bbdd) {
        case '1':
            /**
             * Solo generamos el dump si estamos desplegando, de lo contrario 
             * dejamos el que ya está en el repositorio
             */
            if (despliegueBD == 'Si') {
                console.log('+---------------------------+');
                console.log('|  Generando dump_dev.sql   |');
                console.log('+---------------------------+');

                dump = 'dump_dev.sql';
                host = 'localhost';
                user = 'wordpress';
                password = 'wordpress';
                database = 'wordpress';

            } else {
                console.info('\x1b[32m', '+-----------------------------------------------+');
                console.info('\x1b[32m', '|  No es necesario hacer el volcado de la BBDD  |');
                console.info('\x1b[32m', '+-----------------------------------------------+');
                return fin;
            }
            break;
        case '2':
            console.log('+------------------------------+');
            console.log('|  Generando dump_preprod.sql  |');
            console.log('+------------------------------+');

            dump = 'dump_preprod.sql';

            if (despliegueBD == "Si") {
                host = 'localhost';
                user = 'wordpress';
                password = 'wordpress';
                database = 'wordpress';
            } else {
                host = process.env.DOMINIO_PREPROD;
                user = process.env.PRE_DB_USER;
                password = process.env.PRE_DB_PASSWORD;
                database = process.env.PRE_DB_NAME;
            }
            break;
        case '3':
            console.log('+---------------------------+');
            console.log('|  Generando dump_prod.sql  |');
            console.log('+---------------------------+');

            dump = 'dump_prod.sql';

            if (despliegueBD == "Si") {
                host = 'localhost';
                user = 'wordpress';
                password = 'wordpress';
                database = 'wordpress';
            } else {
                host = process.env.DOMINIO_PROD;
                user = process.env.DB_USER;
                password = process.env.DB_PASSWORD;
                database = process.env.DB_NAME;
            }
            break;
        default:
            console.error('\x1b[31m', '+------------------------------------+');
            console.error('\x1b[31m', '|  No se ha definido la BBDD         |');
            console.error('\x1b[31m', '|  El Dump no se ha podido realizar  |');
            console.error('\x1b[31m', '+------------------------------------+');
            return fin;
            break;

    }

    return mysqlDump({
        connection: {
            host: host,
            user: user,
            password: password,
            database: database,
        },
        dump: {
            schema: {
                table: {
                    dropIfExist: 1,
                    charset: 'utf8mb4_unicode_ci',
                },
            },
        },
        dumpToFile: './' + dump,
    });
}
exports.dumpDatabase = dumpDatabase;

/*
 * Configuración de la tarea 'dockerupUpBD'
 * Levantamos un docker compose distinto
 * donde la BBDD se carga desde un dump sql
 */
function importaBD() {

    /**
     * Esta variable controla si la BBDD que vamos a desplegar es la que ya tenemos o no
     * En caso de ser la que ya tenemos, evitamos tener que importarla,
     * si es distinta, ejecutamos la imprtación
     */
    var bdlocal = 0;

    /*Evaluamos la variable auxiliar que nos indica sobre que BBDD trabajar*/
    if (bbdd == 0) {
        /**
         * La variable se inicia a 0, por lo que si este valor
         * no se ha modificado, buscamos si han pasado argumentos 
         * indicando la acción
         */
        switch (arg.bbdd) {
            case 'dev':
                bbdd = '1';
                bdlocal = true;
                break;
            case 'preprod':
                bbdd = '2';
                break;
            case 'prod':
                bbdd = '3';
                break;
            default:
                console.error('\x1b[31m%s\x1b[0m', '+-----------------------------------------------------------------------------+');
                console.error('\x1b[31m%s\x1b[0m', '|  Argumento inválido, use uno de los siguientes argumentos                   |');
                console.error('\x1b[31m%s\x1b[0m', '+-----------------------------------------------------------------------------+');
                console.error('\x1b[31m%s\x1b[0m', '|  --bbdd dev // Importa el dump de la base de datos local                    |');
                console.error('\x1b[31m%s\x1b[0m', '|  --bbdd preprod // Importa el dump de la base de datos de pre-producción    |');
                console.error('\x1b[31m%s\x1b[0m', '|  --bbdd prod // Importa el dump de la base de datos de producción           |');
                console.error('\x1b[31m%s\x1b[0m', '+-----------------------------------------------------------------------------+');
                break

        }
    }

    /**
     * Evaluamos el valor de la variable bbdd
     * Este valor nos indicará sobre que bbdd
     * hacer el dump
     * 1 - Base de datos del repositorio
     * 2 - Base de datos de Pre - producción
     * 3 - Base de datos de Producción
     */
    switch (bbdd) {
        case '1':
            bdlocal = true;
            break;
        case '2':
            var archivo = 'dump_preprod.sql';
            gulp.src('./' + archivo).pipe(gulp.dest('./code'));
            break;
        case '3':
            var archivo = 'dump_prod.sql';
            gulp.src('./' + archivo).pipe(gulp.dest('./code'));
            break;
        default:
            console.error('\x1b[31m%s\x1b[0m', '+---------------------------------------+');
            console.error('\x1b[31m%s\x1b[0m', '|   No se ha definido la BBDD           |');
            console.error('\x1b[31m%s\x1b[0m', '|   El Dump no se ha podido importar    |');
            console.error('\x1b[31m%s\x1b[0m', '+---------------------------------------+');
            return fin;
            break;
    }

    if (bbdd != 0) {
        /**
         * Solo importamos el dump si estamos la bbdd es distinta q la que ya tenemos,
         * de lo contrario, dejamos el que ya está en el repositorio
         */
        if (!bdlocal) {
            var script = 'docker-compose run cli wp db import ' + archivo;

            console.log('\x1b[33m%s\x1b[0m', '+-----------------------------------+');
            console.log('\x1b[33m%s\x1b[0m', '|     Iniciando la Importación      |');
            console.log('\x1b[33m%s\x1b[0m', '+-----------------------------------+');
            console.log('\x1b[33m%s\x1b[0m', '+  ' + script);


            var exec = require('child_process').exec,
                child;
            child = exec(script);

            return child;
        } else {
            console.log('\x1b[33m%s\x1b[0m', '+-----------------------------------------+');
            console.log('\x1b[33m%s\x1b[0m', '|     No es necesaria la Importación      |');
            console.log('\x1b[33m%s\x1b[0m', '+-----------------------------------------+');
            return fin;
        }
    }
}
exports.importaBD = importaBD;

function sqlcollaction() {

    if (despliegueBD == 'Si') {
        switch (bbdd) {
            case '2':
                var dump = 'dump_preprod.sql';
                break;
            case '3':
                var dump = 'dump_prod.sql';
                break;
            default:
                var dump = 'dump_dev.sql';
                break;
        }
        console.log('+----------------------------------+');
        console.log('|  Preparando SQL para producción  |');
        console.log('|    Modificando Collaction UTF8   |');
        console.log('+----------------------------------+');

        return gulp.src([dump])
            .pipe(replace(/utf8mb4_unicode_520_ci/g, 'utf8mb4_unicode_ci'))
            .pipe(gulp.dest('./'));
    } else {
        console.error('\x1b[31m', '+---------------------------------------------------+');
        console.error('\x1b[31m', '|  No es necesario repasar el collction del a BBDD  |');
        console.error('\x1b[31m', '+---------------------------------------------------+');
        return fin;
    }

}
exports.sqlcollaction = sqlcollaction;


/*
 * Adaptamos la BBDD al nuevo dominio
 */
function configprod() {

    if (despliegueBD == 'No') {
        bbdd = bbdd_aux;
    }


    switch (bbdd) {
        case '2':
            var BD_NAME = process.env.PRE_DB_NAME;
            var BD_USER = process.env.PRE_DB_USER;
            var BD_PASS = process.env.PRE_DB_PASSWORD;
            var BD_HOST = process.env.PRE_DB_HOST;
            break;
        case '3':
            var BD_NAME = process.env.DB_NAME;
            var BD_USER = process.env.DB_USER;
            var BD_PASS = process.env.DB_PASSWORD;
            var BD_HOST = process.env.DB_HOST;
            break;
        default:
            console.error('\x1b[31m', '+---------------------------------------+');
            console.error('\x1b[31m', '|  No se ha definido el origen          |');
            console.error('\x1b[31m', '|  El volcado no se ha podido realizar  |');
            console.error('\x1b[31m', '+---------------------------------------+');
            return fin;
            break;
    }

    console.log('+------------------------------+');
    console.log('|  Modificando conexión BBDD   |');
    console.log('| - Editanto "wp-config.php" - |');
    console.log('|------------------------------|');
    console.log("| - 'DB_NAME', '" + BD_NAME + "'");
    console.log("| - 'DB_USER', '" + BD_USER + "'");
    console.log("| - 'DB_PASSWORD', '" + BD_PASS + "'");
    console.log("| - 'DB_HOST', '" + BD_HOST + "'");
    console.log('+------------------------------+');

    return gulp.src(['./code/wp-config.php'])
        .pipe(replace(/'DB_NAME', 'wordpress'/g, "'DB_NAME', '" + BD_NAME + "'"))
        .pipe(replace(/'DB_USER', 'wordpress'/g, "'DB_USER', '" + BD_USER + "'"))
        .pipe(replace(/'DB_PASSWORD', 'wordpress'/g, "'DB_PASSWORD', '" + BD_PASS + "'"))
        .pipe(replace(/'DB_HOST', 'mysql:3306'/g, "'DB_HOST', '" + BD_HOST + "'"))
        .pipe(gulp.dest('./code'));
}
exports.configprod = configprod;

function importaMedios() {

    switch (bbdd) {
        case '2':
            var urlremota = process.env.PROTOCOLO_PREPROD + process.env.DOMINIO_PREPROD
            break;
        case '3':
            var urlremota = process.env.PROTOCOLO_PROD + process.env.DOMINIO_PROD
            break;
        default:

            break;
    }

    console.log('+------------------------------+');
    console.log('|      Importando Medios       |');
    console.log('|------------------------------|');


    /**
     * Movemos el script a la carpeta Code para poder
     * lanzarla con el WP CLI
     */
    console.log('|  Replicando script           |');
    gulp.src('./importamedios.php')
        .pipe(replace(/dominioremoto/g, urlremota))
        .pipe(gulp.dest('./code'));

    /**
     * Lanzamos el script mediante WP CLI
     */
    var script = "docker-compose run --rm cli wp eval-file importamedios.php";

    console.log('|  Lanzando script             |');
    console.log('|  ' + script);


    var exec = require('child_process').exec,
        child;
    child = exec(script,
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
    return child;
}
exports.importaMedios = importaMedios;

function regeneraImagenes() {
    var exec = require('child_process').exec,
        child;
    child = exec('docker-compose run --rm cli wp media regenerate --yes',
        function(error, stdout, stderr) {
            console.log(stdout);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
    return child;
}
exports.regeneraImagenes = regeneraImagenes;

/***********************************************************************************************************************
 * 
 *    Procesos
 * 
 /*********************************************************************************************************************** */

exports.gitstop = series(gitadd, gitcommit, gitpush);

exports.iniciodev = series(
    carga,
    gitpull,
    async() => {
        const response = await prompts({
            type: 'select',
            name: 'opcion',
            message: 'Que Base de Datos desea utilizar?',
            choices: [
                { title: 'Desarrollo', value: '1' },
                { title: 'Pre-Producción', value: '2' },
                { title: 'Producción', value: '3' }
            ],
        });
        bbdd = response.opcion;
        console.info('Opcion: ', bbdd);
    },
    dumpDatabase,
    importaBD,
    function limpiaImport() {
        /**
         * Eliminamos el script de la carpeta Code para que no se despliegue 
         * Con el proyecto
         */
        console.log('+--------------------------+');
        console.log('|  Limpiando Import        |');
        console.log('+--------------------------+');
        del.sync(['./code/*.sql']);

        return fin;
    },
    dockerup,
    function defineCambioUrl() {
        /**
         * Evaluamos la BBDD seleccionada, y sobre eso definimos 
         * la sustirución de url en la BBDD
         */
        switch (bbdd) {
            case '1':
                arg.url1 = 'dev';
                break;
            case '2':
                arg.url1 = 'preprod';
                break;
            case '3':
                arg.url1 = 'prod';
                break;
        }
        arg.url2 = 'dev';

        return fin;
    },
    bbddCambioUrl,
    importaMedios,
    function limpiascript() {
        /**
         * Eliminamos el script de la carpeta Code para que no se despliegue 
         * Con el proyecto
         */
        console.log('+----------------------+');
        console.log('|  Limpiando script    |');
        console.log('+----------------------+');
        del.sync(['./code/importamedios.php']);

        return fin;
    },
    regeneraImagenes,
    start
);

exports.continuadev = series(carga, gitpull, dockerup, start);

exports.guarda = series(dockerdown, gitadd, gitcommit, gitpush, carga);

exports.despliegue = series(
    async() => {
        const response = await prompts({
            type: 'select',
            name: 'opcion',
            message: 'Desea desplegar la Base de Datos?',
            choices: [
                { title: 'Si', value: 'Si' },
                { title: 'No', value: 'No' }
            ],
        });
        /**
         * Volcamos el resultado de la selección en la
         * variable "despliegue", de ésta forma, las siguientes 
         * tareas, sabrán que hacer con la base de datos
         */
        despliegueBD = response.opcion;
        console.info('Opcion: ', despliegueBD);

    },
    async() => {
        const response = await prompts({
            type: 'select',
            name: 'opcion',
            message: 'Donde Quiere desplegar?',
            choices: [
                { title: 'Pre-Producción', value: '2' },
                { title: 'Producción', value: '3' }
            ],
        });
        /**
         * Iniciamos la BBDD 1 Para hacer el dump de la web de desarrollo
         * en primera instancia  */
        bbdd = '1';
        /**
         * Almacenamos el valor escogido en una variable auxiliar
         */
        bbdd_aux = response.opcion;
        console.info('Opcion: ', bbdd_aux);

    },
    dockerup,
    dumpDatabase,
    function defineDB() {

        /** En caso de despliegue 
         * Debemos de generar los argumentos que va a necesitar 
         * la función de cambiodeURL
         * 
         * url1 - desarrollo
         * url2 - La seleccionada por el usuario
         * 
         * En el caso contrario, mostramos un mensaje y seguimos
         */
        if (despliegueBD == 'Si') {

            /**
             * Le damos su valor a bbdd
             * puesto que ya se ha desviado para sacar la bbdd
             * de desarrollo, el resto del script manda la opción 
             * que escogió el usuario
             */
            bbdd = bbdd_aux;

            arg.url1 = 'dev';
            switch (bbdd_aux) {
                case '2':
                    arg.url2 = 'preprod'
                    break;

                case '3':
                    arg.url2 = 'prod'
                    break;

            }
        } else {
            console.error('\x1b[32m', '+------------------------------------------------------------------+');
            console.error('\x1b[32m', '|  No es necesaria ninguna acción respecto al tratamiento de BBDD  |');
            console.error('\x1b[32m', '+------------------------------------------------------------------+');
        }

        return fin;
    },
    bbddCambioUrl,
    dumpDatabase,
    sqlcollaction,
    function redefineBD() {
        /**
         * Connmutamos los argumentos de las url para revertir el cambio
         * tras haber sacado la copia de la BBDD
         */
        var url_aux = arg.url1;
        arg.url1 = arg.url2;
        arg.url2 = url_aux;
        return fin;
    },
    bbddCambioUrl,
    function limpiaDumps() {
        if (despliegueBD == 'No') {
            console.log('Limpiando dumps');
            del.sync(['./*.sql']);
        } else {
            console.error('\x1b[32m', '+-------------------------------------+');
            console.error('\x1b[32m', '|  No es necesario limpiar los dumps  |');
            console.error('\x1b[32m', '+-------------------------------------+');
        }
        return fin;
    },
    configprod,
    dockerdown,
    gitadd,
    gitcommit,
    gitpush,
    carga
);

exports.default = function() {

    console.info('\x1b[31m\x1b[40m');
    console.info('                                                                                  ');
    console.info('  \u250F\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2513  ');
    console.info('                                                                             \u2503  ');
    console.info('  \u2523\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u252B  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp default\x1b[37m                                                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Muestra la guía de funciones                                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \x1b[4mListado de Procedimientos\x1b[24m                                               \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp start\x1b[37m                                                           \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Inicia los escuchadores, el proceso se detiene con cntrl+c           \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp iniciodev\x1b[37m                                                       \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Encadena las funciones (gitpull, selectorDB, dumpDatabase,           \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       importaBD, limpiaImport, dockerup, defineCambioUrl, bbddCambioUrl,   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       importaMedios, regeneraImagenes, limpiascript, start)                \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp continuadev\x1b[37m                                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Encadena las funciones (gitpull, dockerup, start)                    \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp gitstop\x1b[37m                                                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Encadena las funciones (gitadd, gitcommit, gitpush)                  \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp guarda\x1b[37m                                                          \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Encadena las funciones (dockerdown, gitstop)                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp despliegue\x1b[37m                                                      \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Encadena las funciones (despliegueBD, selectorDB, dockerup,          \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       dumpDatabase, defineDB, bbddcambioUrl, dumpDatabase,                 \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       sqlcollaction, redefineBD, bbddcambioUrl, limpiaDumps,               \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       configprod, gitadd, gitcommit, gitpush)                              \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2523\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u252B  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \x1b[4mListado de Escuchadores\x1b[24m                                                 \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp sass\x1b[37m                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Compila los archivos SASS en CSS                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp scss\x1b[37m                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Compila los archivos SCSS en CSS                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp compactacss\x1b[37m                                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Minimiza los archivos css                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp compactajs\x1b[37m                                                      \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Minimiza los archivos js                                             \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2523\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u252B  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \x1b[4mFunciones para Docker\x1b[24m                                                   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp dockerup\x1b[37m                                                        \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Inicia el contenedor del proyecto                                    \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp dockerdown\x1b[37m                                                      \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Elimina el contenedor del proyecto                                   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2523\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u252B  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \x1b[4mFunciones para Git\x1b[24m                                                      \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp gitpull\x1b[37m                                                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Actualiza el proyecto desde el repositorio remoto                    \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp gitadd\x1b[37m                                                          \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Añade todos los archivos modificados al repositorio                  \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp gitcommit\x1b[37m                                                       \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Confirma los cambios de los archivos del repositorio                 \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp gitpush\x1b[37m                                                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Actualiza el repositorio remoto con el estado local                  \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2523\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u252B  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \x1b[4mFunciones para manejo de Medios\x1b[24m                                         \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp importaMedios\x1b[37m                                                   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Importa los medios remotos que no tienen copia en local              \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp regeneraImagenes\x1b[37m                                                \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Regenera las miniaturas de las imágenes del repositorio de medios    \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2523\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u252B  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \x1b[4mFunciones para manejo de Base de Datos\x1b[24m                                  \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp bbddCambioUrl\x1b[37m                                                   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Se le han de pasar 2 argumentos \x1b[34m--url1\x1b[37m cadena \x1b[34m--url2\x1b[37m cadena          \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Sustituye en BBDD la url1 por la url2                                \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Cadenas posibles                                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       --------------------------------------------------------------       \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mdev\x1b[37m // localhost                                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mpreprod\x1b[37m // Dominio pre-producción indicado en el .env                \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mprod\x1b[37m // Dominio url producción indicado en el .env                   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp dumpDatabase\x1b[37m                                                    \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Se le ha de pasar 1 argumento \x1b[34m--bbdd\x1b[37m cadena                          \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Cadenas posibles                                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       --------------------------------------------------------------       \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mdev\x1b[37m // Realiza un dump de la base de datos local                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mpreprod\x1b[37m // Realiza un dump de la base de datos de pre-producción     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mprod\x1b[37m // Realiza un dump de la base de datos de producción            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp importaBD\x1b[37m                                                       \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Se le ha de pasar 1 argumento \x1b[34m--bbdd\x1b[37m cadena                          \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Cadenas posibles                                                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       --------------------------------------------------------------       \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mdev\x1b[37m // Importa un dump de la base de datos local                     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mpreprod\x1b[37m // Importa un dump de la base de datos de pre-producción     \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       \x1b[34mprod\x1b[37m // Importa un dump de la base de datos de producción            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp sqlcollaction\x1b[37m                                                   \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Revisa el collaction de la base de datos (dump_prod.sql)             \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       modificacndo utf8mb4_unicode_520_ci por utf8mb4_unicode_ci           \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m                                                                            \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m    \u2794  \x1b[32mgulp configprod\x1b[37m                                                      \x1b[31m\u2503  ');
    console.info('  \u2503\x1b[37m       Configura el archivo wp-config con los datos de producción           \x1b[31m\u2503  ');
    console.info('  \u2503                                                                            \u2503  ');
    console.info('  \u2517\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u251B  ');
    console.info('\x1b[0m');

    return fin;
}